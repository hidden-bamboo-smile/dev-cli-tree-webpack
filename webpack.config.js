const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ProgressBarWebpackPlugin = require('progress-bar-webpack-plugin')

module.exports = {
    mode: 'production',//development、production
    entry: path.resolve(__dirname, "./src/js/demo.js"),
    output: {
        path: path.resolve(__dirname, './build'),
        filename: "js/demo.min.js"
    },
    module: {
        rules:[
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
                // test 说明了当前 loader 能处理那些类型的文件
                // use 则指定了 loader 的类型。
                // 注意：数组中的loader不能省略扩展名
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {// 实现 url 资源打包
                // 图片文件使用 url-loader 来处理
                test: /\.(png|jpg|gif|ttf|eot|woff|woff2|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        // options 为可以配置的选项
                        options: {
                            limit: 8192
                            // limit=8192表示将所有小于8kb的图片都转为base64形式
                            // （其实应该说超过8kb的才使用 url-loader 来映射到文件，否则转为data url形式）
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            filename: "index.html",  //将src目录下的index.html为模板生成打包后的webpack.html到 build目录下
            minify: {
                minimize: true, //是否打包为最小值
                removeAttributeQuotes: false,  //去除引号
                removeComments: true,  //去除注释
                collapseWhitespace: true,  //去除空格
                minifyCSS: true,  //压缩html内的样式
                minifyJS: true, //压缩html内的js
                removeEmptyElements: false //清理内容为空的元素
            },
            hash: true  //引入产出资源的时候加上哈希避免缓存
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: __dirname+'/src/assets', to: __dirname+'/build/assets' },
            ],        
        }),
        new ProgressBarWebpackPlugin()
    ]
};