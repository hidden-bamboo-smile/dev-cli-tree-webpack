目录结构说明：
```
-build/              打包发布文件目录
 -node_modules/       node开发依赖模块
 -src/                源文件路径
 --index.html         入口页面html
 --assets             静态文件夹，该文件夹内容及结构会随webpack打包原样复制到build目录，用于保证第三方插件及静态文件的正确引用
 --css                这里的css/*.css文件需要在src/js/demo.js文件里面引入,将被打包进html文件
 --js                 只有被引用的js文件会被打包
  ---demo.js         入口js文件
-.babelrc            es6 es5 语法转换插件配置文件
-package.json        node项目相关配置文件
-webpack.config.js   webpack打包插件相关配置文件
```

使用：
1. npm install
2. npm run mywebpack打包html
3. npm run pkgwin 打包exe后台，需要pkg支持