require('../css/demo.css')
const $ = require('./jquery-3.3.1.min.js');


$(document).ready(function(){
    start();
});

var tabletable;
function start() {
    layui.use(['layer', 'form','element'], function(){
        var layer = layui.layer
        ,form = layui.form
        ,element = layui.element,
        table = layui.table;
        tabletable = table;
        //监听提交
        form.on('submit(formDemo)', function(data){
            console.log(data)
            // layer.msg(JSON.stringify(data.field));
            let tt = {};
            for (const key in data.field) {
                tt[key] = parseFloat(data.field[key]);
            }
            
            console.log(tt);
            GETDATA(tt);
            return false;
        });

        table.render({
            elem: '#test'
            ,toolbar: true //开启头部工具栏
            ,defaultToolbar: ['exports', 'print']
            ,title: '选型结果表'
            ,cols: [[
                {field:'id', title:'结果', width: 100 ,align:'center'}
                ,{field:'content', title:'详细内容', edit: 'text', align:'center'}
            ]]
            ,page: false
            ,data: [
                {"id":"无型号","content":"请设定参数"}
            ]
            ,text: {
                none: '无优选型号' //默认：无数据。
            }
        });
        
        layer.msg('使用方法：参数设定--> 立即提交--> 选型结果查看');
    });
    layui.config({
        version: false //一般用于更新模块缓存，默认不开启。设为 true 即让浏览器不缓存。
        ,debug: false //用于开启调试模式，默认 false，如果设为 true，则JS模块的节点会保留在页面
        ,base: '' //设定扩展的 layui 模块的所在目录，一般用于外部模块扩展
    });
    var device = layui.device();
    console.log(device);

    $('.select_set').click(function () {
        console.log('select_set');
        // $('#content').css('display','block');
        // $('#content_result').css('display','none');
        // $('.select_result').removeClass('layui-this');
        if ($('#content').css('display') == "block") {
            $('#content').css('display','none');
            $('.select_set').removeClass('layui-this');
        }else{
            $('#content').css('display','block');
            
        }
    });
    $('.select_result').click(function () {
        console.log('select_result');
        // $('#content').css('display','none');
        // $('#content_result').css('display','block');
        // $('.select_set').removeClass('layui-this');
        if ($('#content_result').css('display') == "block") {
            $('#content_result').css('display','none');
            $('.select_result').removeClass('layui-this');
        }else{
            $('#content_result').css('display','block');
            
        }
    });
    $("input").blur(function(){
        $(this).val(isNaN(parseFloat($(this).val()))?'':parseFloat($(this).val()).toFixed(3));
        console.log($(this).val());
    });
}
//测试
function iop() {
    let result = {
        "status" : "",
        "message": "",
        "data"   :{
            "ex" : ["最优选型号1","最优选型号1","最优选型号1","最优选型号1"],
            "per" : ["优选型号1","优选型号1","优选型号1","优选型号1"],
            // "over": "单位转速不合", //只有不和的时候才返回这个文字 反之为空
            // "unw" : "水头段不合"    //只有不合的时候才返回这个文字 反之为空
        }
    }
    let Data = [];
    let nodata = "" ;
    for (const key in result.data) {
        let tempdic = {};
        if (key != "over" && key != "unw") {
            if (key == "ex" && Array.isArray(result.data[key]) && result.data[key].length>0 ) {
                tempdic['id'] = "最优选";
                tempdic['content'] = "";
                for (let index = 0; index < result.data[key].length; index++) {
                    tempdic['content'] += result.data[key][index] + "; ";
                    
                }
                tempdic['content'] = tempdic['content'].slice(0,tempdic['content'].length-2);

            } else if (key == "per" && Array.isArray(result.data[key]) && result.data[key].length>0 ){
                tempdic['id'] = "优选";
                tempdic['content'] = "";
                for (let index = 0; index < result.data[key].length; index++) {
                    tempdic['content'] += result.data[key][index] + "; ";
                }
                tempdic['content'] = tempdic['content'].slice(0,tempdic['content'].length-2);

            }
            Data.push(tempdic);
        } else {
            nodata += result.data[key] +'; '

        }

    }
    if (nodata.length>0) {
        nodata = nodata.slice(0,nodata.length-2)
        Data = [{
            "id":"无型号",
            "content":nodata
        }];
    }
    tabletable.reload('test', {
        data: Data
    });
}


function GETDATA(data) {
    //loading
    layer.load(3, {
        shade: [0.5,'#fff'] //0.1透明度的白色背景
      ,shadeClose : true
      });   
    console.log('GETDATA');

    $.ajax({
        url: "http://192.168.4.178:9898/SelMod",
        type: "GET",
        async: true,
        timeout: 10000,
        contentType: "application/json;charset=UTF-8",
        data: data,
        // data: {
        //     "N" : "发电机出力",
        //     "ng": "发电机效率", 
        //     "H" : "水头" ,
        //     "F" : "单元流量",
        //     "np": "水轮机效率",
        //     "time":new Date().Format("yyyy-MM-dd HH:mm:ss")
        // },
        success: function (result) {
            // {
            //     "status" : "",
            //     "message": "",
            //     "data"   :{
            //         "ex" : ["最优选型号1","最优选型号1","最优选型号1","最优选型号1"],
            //         "per" : ["优选型号1","优选型号1","优选型号1","优选型号1"],
            //         "over": "单位转速不合", //只有不和的时候才返回这个文字 反之为空
            //         "unw" : "水头段不合"    //只有不合的时候才返回这个文字 反之为空
            //     }
            // }
            result = JSON.parse(result);
            console.log(result);
            if (result.status == 0) {
                let Data = [];
                let nodata = "" ;
                for (const key in result.data) {
                    let tempdic = {};
                    if (key != "over" && key != "unw") {
                        if (key == "ex" && Array.isArray(result.data[key]) && result.data[key].length>0 ) {
                            tempdic['id'] = "最优选";
                            tempdic['content'] = "";
                            for (let index = 0; index < result.data[key].length; index++) {
                                tempdic['content'] += result.data[key][index] + "; ";
                                
                            }
                            tempdic['content'] = tempdic['content'].slice(0,tempdic['content'].length-2);

                        } else if (key == "per" && Array.isArray(result.data[key]) && result.data[key].length>0 ){
                            tempdic['id'] = "优选";
                            tempdic['content'] = "";
                            for (let index = 0; index < result.data[key].length; index++) {
                                tempdic['content'] += result.data[key][index] + "; ";
                            }
                            tempdic['content'] = tempdic['content'].slice(0,tempdic['content'].length-2);

                        }
                        Data.push(tempdic);
                    } else {
                        nodata += result.data[key] +'; '

                    }

                }
                if (nodata.length>0) {
                    nodata = nodata.slice(0,nodata.length-2)
                    Data = [{
                        "id":"无型号",
                        "content":nodata
                    }];
                }
                tabletable.reload('test', {
                    data: Data
                });
            } else {
                alert("！错误信息："+result.message);
            }
            //unloading
            layer.closeAll('loading');
        },
        error: function (e) {
            alert("无法连接到后台服务器！请检查网络连接");
            layer.closeAll('loading');
        }
    });
}